﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arreglos
{
    class Program
    {
        static void Main(string[] args)
        {
            var arreglo = new float[5];

            // Recorrer un arreglo utilizando for
            Console.WriteLine("Uso de for...");
            for (int i = 0; i < arreglo.Length; i++)
            {
                Console.Write($"{arreglo[i]} - ");
            }

            // Recorrer un arreglo utilizando foreach
            Console.WriteLine("\nUso de foreach");
            foreach (int elemento in arreglo)
            {
                Console.Write($"{elemento} - ");
            }

            // Otra forma de construir un arreglo
            var dias = new string[7] { "domingo", "lunes", "martes", "miercoles", "jueves", "viernes", "sabado" };

            // Es posible crear arreglos multidimensionales
            var arreglo2 = new int[3][] { new int[3]{ 1, 2, 3 }, new int[3]{ 4, 5, 6 }, new int[3]{ 7, 8, 9 } };
            Console.Write("\nAcceder a un arreglo bidimensional...");
            Console.WriteLine(arreglo2[1][1]);  // debe imprimir cinco

            Console.ReadKey();
        }
    }
}
